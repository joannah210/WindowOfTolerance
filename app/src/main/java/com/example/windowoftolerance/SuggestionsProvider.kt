package com.example.windowoftolerance

import java.util.*

class SuggestionsProvider {


    fun provideSuggestions(mentalState : Int) : List<String> {
        return filterListSuggestions(
            getMentalStateString(mentalState),
            // TODO write and read from database
            TempSugList().createTempList()
        )
    }

    // TODO make enum
    private fun getMentalStateString(mentalStateInt : Int) : String {
        return when {
            mentalStateInt < 21        -> "Very low"
            mentalStateInt in 21..40   -> "Low"
            mentalStateInt in 41..60   -> "Good"
            mentalStateInt in 61..80   -> "High"
            mentalStateInt > 80        -> "Very high"
            else                       -> ""
        }
    }


    // TODO filter on mentalStateString
    private fun filterListSuggestions(mentalStateString: String, list: List<Suggestion>) : List<String>{
        val filteredList : MutableList<String> = mutableListOf();

        for (item in list) {
            filteredList.add(item.text)
        }

        return filteredList
    }
}
