package com.example.windowoftolerance

class Suggestion constructor (
    val id: Int,
    val text: String,
    val veryLow: Boolean = false,
    val low: Boolean = false,
    val good: Boolean = false,
    val high: Boolean = false,
    val veryHigh: Boolean = false
) {}