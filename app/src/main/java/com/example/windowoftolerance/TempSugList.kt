package com.example.windowoftolerance

class TempSugList () {

    fun createTempList() : MutableList<Suggestion>  {
        val tempSugList : MutableList<Suggestion> = mutableListOf()
        // 1 for all
        tempSugList.add(Suggestion(
            1,
            "One for all!!",
            veryLow = true,
            low = true,
            good = true,
            high = true,
            veryHigh = true
        ))

        // 1 for very low
        tempSugList.add(Suggestion(
            2,
            "Very low",
            veryLow = true
        ))

        // 1 for low
        tempSugList.add(Suggestion(
            3,
            "Low",
            low = true
        ))


        // 1 for high
        tempSugList.add(Suggestion(
            4,
            "High",
            low = true
        ))

        // 1 for very high
        tempSugList.add(Suggestion(
            5,
            "Very high",
            veryLow = true
        ))

        return tempSugList
    }
}