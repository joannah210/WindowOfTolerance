package com.example.windowoftolerance

import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.drawable.toBitmap
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Build and set the gradients
        val rainbow : GradientDrawable = buildRainbowGradient()
        rainbowConstraintLayout.background = rainbow
        suggestionsStrokeConstraintLayout.background = rainbow

        //val verticalSeekbarProgress = verticalSeekbar.progress
        // val pixelY = translateToPixelY(verticalSeekbarProgress)
        // setPointerColor(pixelY, rainbow)

        // TODO format the list
        suggestionsTextView.text =
            SuggestionsProvider().provideSuggestions(verticalSeekbar.progress).toString()

    }

    private fun buildRainbowGradient() : GradientDrawable {
        
        val color = IntArray(7)
            color[0] = getColor(R.color.burnt_sienna)
            color[1] = getColor(R.color.sandy_brown)
            color[2] = getColor(R.color.orange_yellow_crayola)
            color[3] = getColor(R.color.persian_green)
            color[4] = getColor(R.color.charcoal)
            color[5] = getColor(R.color.raisin_black)
            color[6] = getColor(R.color.eerie_black)

        val rainbow = GradientDrawable(
            GradientDrawable.Orientation.TOP_BOTTOM,
            color
        )

        rainbow.cornerRadius = 15.0F

        return rainbow
    }

    private fun translateToPixelY(y : Int) : Int {

        return when {
            y == 50 -> 50
            y <= 50 -> 50 + 15
            y >= 50 -> 50 - 15
            else -> 0
        }
    }

    private fun setPointerColor(pixelY : Int, gradientDrawable: GradientDrawable) {

        val bitmap: Bitmap = gradientDrawable.toBitmap(2, 100)
        val pixel = bitmap.getPixel(1, pixelY)

        val redValue: Int = Color.red(pixel)
        val greenValue: Int = Color.green(pixel)
        val blueValue: Int = Color.blue(pixel)

        println("Red: $redValue")
        println("Green: $greenValue")
        println("Blue: $blueValue")

    }

}